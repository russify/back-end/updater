package endpoint

import (
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"updater/internal/storage"
	"updater/internal/utility"
)

type Handler struct {
	storage *storage.FileStorage
}

func NewHandler() *Handler {
	return &Handler{storage: storage.NewStorage()}
}

func (handler *Handler) Handle(ctx *fasthttp.RequestCtx) {
	defer func() {
		err := recover()
		if err != nil {
			logrus.Error("Panic in handler: ", err)
			ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		}
	}()

	switch utility.ByteArrayToString(ctx.Path()) {
	case "/api/v0/version":
		if fasthttp.MethodGet != utility.ByteArrayToString(ctx.Method()) {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
			return
		}
		handler.storage.Version(ctx)
	case "/api/v0/download":
		if fasthttp.MethodGet != utility.ByteArrayToString(ctx.Method()) {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
			return
		}
		handler.storage.ReadFile(ctx)
	case "/api/v0/upload":
		if fasthttp.MethodPost != utility.ByteArrayToString(ctx.Method()) {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
			return
		}
		handler.storage.SaveFile(ctx)
	default:
		ctx.SetStatusCode(fasthttp.StatusNotFound)
	}
}
