package utility

import (
	"reflect"
	"unsafe"
)

func ByteArrayToString(data []byte) string {
	return *(*string)(unsafe.Pointer(&data))
}

func StringToByteArray(str string) []byte {
	return (*[0x7fff0000]byte)(unsafe.Pointer((*reflect.StringHeader)(unsafe.Pointer(&str)).Data))[:len(str):len(str)]
}
