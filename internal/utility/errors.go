package utility

import "strings"

func ErrorToJson(err error) string {
	return "{\"error\": \"" + err.Error() + "\"}"
}

func ErrorStringToJson(err string) string {
	builder := strings.Builder{}
	builder.Grow(15 + len(err))
	builder.WriteString("{\"error\": \"")
	builder.WriteString(err)
	builder.WriteString("\"}")
	return builder.String()
}
