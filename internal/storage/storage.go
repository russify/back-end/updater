package storage

import (
	"bytes"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"io"
	"os"
	"strings"
	"sync"
	"updater/internal/utility"
)

type FileStorage struct {
	guard       sync.RWMutex
	exeFilePath string
	data        []byte

	meta *viper.Viper
}

func NewStorage() *FileStorage {
	filePath := viper.GetString("storage.windows.executable")
	data, err := os.ReadFile(filePath)
	if err != nil {
		logrus.Fatal("Cannot read executable file: ", err.Error())
	}

	metaFilePath := viper.GetString("storage.windows.meta")
	meta := viper.New()
	meta.SetConfigType("yaml")

	index := strings.LastIndex(metaFilePath, "/")
	if index != -1 {
		meta.AddConfigPath(metaFilePath[:index])
	} else {
		meta.AddConfigPath(".")
	}
	if index == -1 {
		meta.SetConfigName(metaFilePath)
	} else {
		meta.SetConfigName(metaFilePath[index+1:])
	}

	if err = meta.ReadInConfig(); err != nil {
		logrus.Fatal("Failed to read meta file: ", err.Error())
	}

	return &FileStorage{
		meta:        meta,
		data:        data,
		exeFilePath: filePath,
	}
}

func (storage *FileStorage) Version(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("application/json")
	ctx.SetBodyString("{\"version\": \"" + storage.meta.GetString("version") + "\"}")
	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (storage *FileStorage) SaveFile(ctx *fasthttp.RequestCtx) {
	form, err := ctx.MultipartForm()
	if err != nil {
		ctx.Error(utility.ErrorToJson(err), fasthttp.StatusBadRequest)
		return
	}

	version := strings.TrimSpace(form.Value["version"][0])
	if len(version) == 0 {
		ctx.Error(utility.ErrorStringToJson("version must be present"), fasthttp.StatusBadRequest)
		return
	}

	header, err := ctx.FormFile("data")
	if err != nil {
		ctx.Error(utility.ErrorToJson(err), fasthttp.StatusBadRequest)
		return
	}

	file, err := header.Open()
	defer func() {
		if err := file.Close(); err != nil {
			logrus.Error("Cannot close file: ", err.Error())
		}
	}()

	buffer := new(bytes.Buffer)
	if _, err = io.Copy(buffer, file); err != nil {
		ctx.Error(utility.ErrorToJson(err), fasthttp.StatusInternalServerError)
		return
	}
	storage.meta.Set("version", version)
	ctx.SetStatusCode(fasthttp.StatusNoContent)

	// release request
	go func() {
		storage.guard.Lock()
		storage.data = buffer.Bytes()
		storage.guard.Unlock()

		storage.guard.RLock()
		if err := os.WriteFile(storage.exeFilePath, storage.data, 0666); err != nil {
			logrus.Error("Cannot save file to file storage: ", err.Error())
			storage.guard.RUnlock()
			return
		}
		storage.guard.RUnlock()

		if err = storage.meta.WriteConfig(); err != nil {
			logrus.Error("Cannot update version")
			return // skip file saving
		}
	}()
}

func (storage *FileStorage) ReadFile(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("multipart/mixed")
	storage.guard.RLock()
	if _, err := ctx.Write(storage.data); err != nil {
		ctx.Error(utility.ErrorToJson(err), fasthttp.StatusInternalServerError)
		return
	}
	storage.guard.RUnlock()
	ctx.SetStatusCode(fasthttp.StatusOK)
}
