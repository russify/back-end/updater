package main

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"os"
	"os/signal"
	"updater/internal/endpoint"
	"updater/internal/logger"
)

func main() {
	logger.Initialize()
	initConfig()

	go func() {
		logrus.Info("Starting server")

		server := fasthttp.Server{
			Handler:           endpoint.NewHandler().Handle,
			StreamRequestBody: true,
		}

		err := server.ListenAndServe(":" + viper.GetString("server.port"))
		if err != nil {
			logrus.Error(err)
		}
	}()

	termSig := make(chan os.Signal, 1)
	signal.Notify(termSig, os.Interrupt)
	<-termSig
}

func initConfig() {
	viper.AddConfigPath(".")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	if err := viper.ReadInConfig(); err != nil {
		logrus.Fatal("Failed to read config: ", err.Error())
	}
}
